<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../config/dbConfig.php';
require '../sqlmanager/SQLManager.php';

$app = new \Slim\App;
$app->get('/flights', function (Request $request, Response $response, array $args) {
    require '../config/dbConfig.php';
    $sqlmanager = new SQLManager($dbConfig["host"], $dbConfig["user"], $dbConfig["pass"], $dbConfig["database"]);

    if($flights = $sqlmanager->executeQuery("flights", "SELECTALL")){
        return $response->withJson($flights);
    }else{
        $response->withJson(null);
    }
});

$app->get('/myflights/{uid}', function (Request $request, Response $response, array $args) {
    require '../config/dbConfig.php';
    $sqlmanager = new SQLManager($dbConfig["host"], $dbConfig["user"], $dbConfig["pass"], $dbConfig["database"]);

    $sqlmanager->parameters = array(
        "uid" => $args['uid']
    );

    if($flights = $sqlmanager->executeQuery("flights", "SELECTBYUSER")){
        return $response->withJson($flights);
    }else{
        $response->withJson(null);
    }
});

$app->get('/info', function (Request $request, Response $response, array $args) {
    require '../config/info.php';
    return $response->withJson($infoPochinkiAir);
});

$app->post('/insert', function (Request $request, Response $response) {
    require '../config/dbConfig.php';
    $sqlmanager = new SQLManager($dbConfig["host"], $dbConfig["user"], $dbConfig["pass"], $dbConfig["database"]);

    $sqlmanager->parameters = $request->getParsedBody();

    if($flights = $sqlmanager->executeQuery("flights", "INSERTPURCHASE")){
        return $response;
    }
});

$app->run();