-- Adminer 4.6.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `pochinkiair` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `pochinkiair`;

DELIMITER ;;

DROP PROCEDURE IF EXISTS `getFlights`;;
CREATE PROCEDURE `getFlights`()
BEGIN

SELECT f.`from`, f.destination, 
f.departure_time AS 'departureTime', 
f.arrival_time AS 'arrivalTime', 
f.`date`, f.seats, (f.seats - (
	SELECT COUNT(p.FirUID)
	FROM purchases AS p
	WHERE p.idFlights = f.id
)) AS 'freeSeats', f.price
FROM flights AS f;

END;;

DELIMITER ;

DROP TABLE IF EXISTS `flights`;
CREATE TABLE `flights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `destination` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `departure_time` time NOT NULL,
  `arrival_time` time NOT NULL,
  `date` date NOT NULL,
  `seats` int(11) NOT NULL,
  `price` float(6,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `flights` (`id`, `from`, `destination`, `departure_time`, `arrival_time`, `date`, `seats`, `price`) VALUES
(2,	'Madrid',	'Pochinki',	'08:00:00',	'15:00:00',	'2018-04-04',	100,	50.00),
(3,	'Pochinki',	'Madrid',	'10:00:00',	'18:00:00',	'2018-04-15',	100,	45.00);

DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFlights` int(11) NOT NULL,
  `FirUID` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  `seat` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idFlights` (`idFlights`),
  CONSTRAINT `purchases_ibfk_2` FOREIGN KEY (`idFlights`) REFERENCES `flights` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `purchases` (`id`, `idFlights`, `FirUID`, `seat`) VALUES
(2,	2,	'WDDPnK1xn8Vp3GonngpKM9jzxQS2',	15),
(3,	3,	'WDDPnK1xn8Vp3GonngpKM9jzxQS2',	25);

-- 2018-03-04 07:59:41
