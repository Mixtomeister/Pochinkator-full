<?php

class SQLManager{
    public $querys, $parameters;

    private $db_user, $db_pass, $db_name, $db_url;

    function __construct($db_url, $db_user, $db_pass, $db_name){
        $this->db_url = $db_url;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_name = $db_name;
        $this->loadQuerys();
    }

    function loadQuerys(){
        $myfile = fopen("../sqlmanager/querys.json", "r") or die("Unable to open file!");
        $this->querys = json_decode(fread($myfile,filesize("../sqlmanager/querys.json")), true);
        fclose($myfile);
    }

    function executeQuery($table, $action){
        $arrRes = array();
        $arrResQuery = array();
        $mysqli = new mysqli($this->db_url, $this->db_user, $this->db_pass, $this->db_name);
        $query = $this->querys[$table][$action];
        if(strpos($query, "$$")){
            $query = $this->parseString($query);
        }
        if($res = $mysqli->query($query)){
            if(!($res === true)){
                while($column = $res->fetch_field()){
                    $arrColumn[] = $column->name;
                }
                for($i = 0; $row = $res->fetch_array(MYSQLI_ASSOC); $i++){
                    foreach($arrColumn as $col){
                        $arrResQuery[$i][$col] = $row[$col];
                    }
                }
            }
        }else{
            return false;
        }
        return $arrResQuery;
    }

    private function parseString($query){
       while($posInit = strpos($query, "$$")){
            $posEnd = strpos($query, "$$", $posInit + 1) + 2;
            $param = substr($query, $posInit + 2, ($posEnd - $posInit) - 4);
            $param = explode(":", $param);
            if($param[1] === "String"){
                $parameter = "'". $this->parameters[$param[0]] ."'";    
            }else{
                $parameter = $this->parameters[$param[0]];
            }
            $query = str_replace(substr($query, $posInit, ($posEnd - $posInit)), $parameter, $query);
        }
        return $query;
    }
}

?>