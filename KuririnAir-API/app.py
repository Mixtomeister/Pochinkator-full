from flask import Flask, request, jsonify, render_template, redirect
from flask_cors import CORS
import json
import os
from bson import ObjectId
from pymongo import MongoClient

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1600 * 1024 * 1024
cors = CORS(app,resources=r'/api/*')
class MongoDBManager:
    def __init__(self):
        self.client = MongoClient('mongodb://localhost:27017/')
        self.db = self.client['KuririnAir']
        self.collectionFlights = self.db['flights_api']
        self.collectionInfo = self.db['info_api']
    
    def getInfo(self):
        return jsonify(list(self.collectionInfo.find({}, {'_id': False})))

    def getCollection(self):
        return jsonify(list(self.collectionFlights.find({}, {'_id': False,'clientes':False})))

    def getFlight(self,filter,flight):
        return jsonify(list(self.collectionFlights.find({filter:flight}, {'_id': False,'clientes':False})))

    def getMyFlight(self,uid):
        return jsonify(list(self.collectionFlights.find({'clientes.UID':uid},{'_id': False,'clientes':False})))
        
    def getFlightByFilter(self,jsonVal):
        data = json.loads(jsonVal)
        return jsonify(list(self.collectionFlights.find({"from":data["from"],"destination":data["destination"],"date":data["date"]}, {'_id': False,'clientes':False})))

    def insertClient(self,jsonVal):
        data = json.loads(jsonVal)
        listFlight = list(self.collectionFlights.find({"id":int(data["id"])}, {'_id': False}))
        freeSeats = int(listFlight[0]["freeSeats"])
        self.collectionFlights.update_one(
            {"id":int(data["id"])},
            {
                "$push":
                    {"clientes":
                        {
                            "UID":str(data["uid"]),
                            "seat":int(freeSeats)
                        }
                    },
                "$inc":
                    {
                        "freeSeats": -1
                    }
            }
            ,upsert=False)
        
    def removeClient(self,jsonVal):
        data = json.loads(jsonVal)
        self.collectionFlights.update_one(
            {"id":int(data["id"])},
            {
                "$pull":
                    {"clientes":
                        {
                            "UID":str(data["uid"]),
                        }
                    },
                "$inc":
                    {
                        "freeSeats": +1
                    }
            },
            upsert=False)

@app.route('/api/info')
def info():
    managerMongo = MongoDBManager()
    return managerMongo.getInfo()

@app.route('/api/filter', methods=['GET','POST'])
def filter():
    if request.method == 'POST':
        managerMongo = MongoDBManager()
        jsonFilter = json.dumps({"from":request.form['paramFrom'],"destination":request.form['paramDest'],"date":request.form['paramDate']})
        managerMongo.getFlightByFilter(jsonFilter)
    return render_template('form.html')

@app.route('/api/myflights/<string:uid>')
def searchMyFlight(uid):
    managerMongo = MongoDBManager()
    return managerMongo.getMyFlight(uid)


@app.route('/api/insert', methods=['GET','POST','OPTIONS'])
def insert():
    if request.method == 'POST':
        managerMongo = MongoDBManager()
        jsonArgs = request.get_json()
        jsonFilter = json.dumps({"id":jsonArgs['id'],"uid":jsonArgs['uid']})
        managerMongo.insertClient(jsonFilter)
    return render_template('update.html')

@app.route('/api/remove', methods=['GET','POST'])
def remove():
    if request.method == 'POST':
        managerMongo = MongoDBManager()
        jsonFilter = json.dumps({"id":request.json['id'],"uid":request.json['uid']})
        managerMongo.removeClient(jsonFilter)
    return render_template('remove.html')

@app.route('/api/flights/<string:filter>/<string:flight>')
def search(filter,flight):
    managerMongo = MongoDBManager()
    return managerMongo.getFlight(filter,flight)

@app.route('/api/flights')
def index():
    managerMongo = MongoDBManager()
    return managerMongo.getCollection()

if __name__ =='__main__':
    app.run(host='0.0.0.0',debug=True)